BINARY = crossroad
GOARCH = amd64

COMMIT=$(shell git rev-parse HEAD)
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

BUILD=0
VERSION=$(shell cat VERSION).${BUILD}
IMAGE=ame/crossroad

REGISTRY_FROM=registry.avisi.cloud

PKG_LIST := $(shell go list ./...)

LDFLAGS = -ldflags "-X main.version=${VERSION} -X main.commit=${COMMIT} -X main.branch=${BRANCH}"

compile: build

linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=${GOARCH} go build -a ${LDFLAGS} -o ./bin/${BINARY}-linux-${GOARCH} . ;

darwin:
	CGO_ENABLED=0 GOOS=darwin GOARCH=${GOARCH} go build -a ${LDFLAGS} -o ./bin/${BINARY}-darwin-${GOARCH} . ;

build:
	CGO_ENABLED=0 GOARCH=${GOARCH} go build -a ${LDFLAGS} -o ./bin/${BINARY} . ;

test:
	go test -v ./... -cover

fmt:
	go fmt ${PKG_LIST};

lint:
	golint ${PKG_LIST}

race:
	@go test -p 1 -race -short ${PKG_LIST}

vet:
	go vet ${PKG_LIST};

ci: docker-info docker up-integration version

docker-info:
	docker info

docker:
	docker build -t ${REGISTRY_FROM}/${IMAGE}:${VERSION}${BRANCH} .

up:
	docker-compose up -d

down:
	docker-compose down

clean:
	-rm -f ${BINARY}-*

.PHONY: compile linux darwin build test install fmt lint vet docker-info docker promote up down clean promote version
