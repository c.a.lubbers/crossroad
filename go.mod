module gitlab.avisi.cloud/ame/crossroad

go 1.17

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/gorilla/mux v1.8.0
	gopkg.in/yaml.v2 v2.4.0
)
